package com.servlet;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.entity.Student;
import com.service.StudentService;
import com.util.DataPacket;
import com.util.UploadFileCom;

public class StudentServlet extends HttpServlet {
	private StudentService service = new StudentService();
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String m = request.getParameter("method");
		if ("validName".equals(m)) {
			validName(request,response);
		}
		else if ("add".equals(m)) {
			add(request,response);
		}
		else if ("list".equals(m)) {
			list(request,response);
		}
		else if ("getById".equals(m)) {
			getById(request,response);
		}
		else if ("update".equals(m)) {
			update(request,response);
		}
	}

	private void update(HttpServletRequest request, HttpServletResponse response) throws IOException {
		DataPacket dataPacket = UploadFileCom.upload(request);
		String id = dataPacket.getValue("id");
		String photoPath = dataPacket.getValue("photoPath");
		String name = dataPacket.getValue("name");
		String age = dataPacket.getValue("age");
		String birthday = dataPacket.getValue("birthday");
		StringBuffer buffer = new StringBuffer();
		String[] hs = dataPacket.getValues("hobbies");
		for (String h : hs) {
			buffer.append(",").append(h);
		}
		String hobbies = buffer.substring(1).toString();
		String sex = dataPacket.getValue("sex");
		Student student = new Student(Integer.parseInt(id),name,Integer.parseInt(age), birthday, hobbies, sex,photoPath);
		String fileName = dataPacket.getFileName("photo");
		if (null !=fileName && !"".equals(fileName.trim())) {
			boolean b = dataPacket.write("photo", "/upload/photo/");
			if (b) {
				student.setPhotoPath("upload/photo/"+fileName);
			}
		}
	boolean flag= service.update(student);
	response.getWriter().print(flag);
	}

	private void getById(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String id = request.getParameter("id");
		Student student= service.getById(Integer.parseInt(id));
		JSONObject json = JSONObject.fromObject(student);
		response.getWriter().print(json);
	}

	private void list(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<Student>list = service.list();
		JSONArray array = JSONArray.fromObject(list);
		response.getWriter().print(array);
	}

	private void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
		DataPacket dataPacket = UploadFileCom.upload(request);
		String name = dataPacket.getValue("name");
		String age = dataPacket.getValue("age");
		String birthday = dataPacket.getValue("birthday");
		StringBuffer buffer = new StringBuffer();
		String[] hs = dataPacket.getValues("hobbies");
		for (String h : hs) {
			buffer.append(",").append(h);
		}
		String hobbies = buffer.substring(1).toString();
		String sex = dataPacket.getValue("sex");
		Student student = new Student(name,Integer.parseInt(age), birthday, hobbies, sex);
		String fileName = dataPacket.getFileName("photo");
		if (null !=fileName && !"".equals(fileName.trim())) {
			boolean b = dataPacket.write("photo", "/upload/photo/");
			if (b) {
				student.setPhotoPath("upload/photo/"+fileName);
			}
		}
	boolean flag= service.add(student);
	response.getWriter().print(flag);
	}

	private void validName(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String name = request.getParameter("name");
		boolean flag = service.validName(name);
		response.getWriter().print(flag);
	}
}
