package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.entity.Student;

public class StudentDao extends BaseDao {

	public boolean validName(String name) {
		Connection conn = getConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;	
		try {
			 ps = conn.prepareStatement("select * from student where name=?");
			ps.setString(1, name);
			 rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs, ps, conn);
		}
		return true;
	}

	public boolean add(Student student) {
		Connection connection = getConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			 ps = connection.prepareStatement("insert into student(name,age,birthday,hobbies,sex,photoPath) values(?,?,?,?,?,?)");
			ps.setString(1, student.getName());
			ps.setInt(2, student.getAge());
			ps.setString(3, student.getBirthday());
			ps.setString(4, student.getHobbies());
			ps.setString(5, student.getSex());
			ps.setString(6, student.getPhotoPath());
			int i = ps.executeUpdate();
			return i>0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs, ps, connection);
		}
		return false;
	}

	public List<Student> list() {
		Connection connection = getConnection();
		List<Student> list = new ArrayList<Student>();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			 ps = connection.prepareStatement("select * from student");
			 rs = ps.executeQuery();
			while (rs.next()) {
				list.add(new Student(
						rs.getInt("id"),
						rs.getString("name"),
						rs.getInt("age"),
						rs.getString("birthday"),
						rs.getString("hobbies"),
						rs.getString("sex"),
						rs.getString("photoPath")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs, ps, connection);
		}
		return list;
	}

	public Student getById(int id) {
		Connection connection = getConnection();
		Student student=null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			 ps = connection.prepareStatement("select * from student where id=?");
			 ps.setInt(1, id);
			 rs = ps.executeQuery();
			if (rs.next()) {
				student=new Student(
						rs.getInt("id"),
						rs.getString("name"),
						rs.getInt("age"),
						rs.getString("birthday"),
						rs.getString("hobbies"),
						rs.getString("sex"),
						rs.getString("photoPath"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs, ps, connection);
		}
		return student;
	}

	public boolean update(Student student) {
		Connection connection = getConnection();
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			 ps = connection.prepareStatement("update student set name=?,age=?,birthday=?,hobbies=?,sex=?,photoPath=? where id=?");
			ps.setString(1, student.getName());
			ps.setInt(2, student.getAge());
			ps.setString(3, student.getBirthday());
			ps.setString(4, student.getHobbies());
			ps.setString(5, student.getSex());
			ps.setString(6, student.getPhotoPath());
			ps.setInt(7, student.getId());
			int i = ps.executeUpdate();
			return i>0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			close(rs, ps, connection);
		}
		return false;
	}
}
