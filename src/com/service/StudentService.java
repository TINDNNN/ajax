package com.service;

import java.util.List;

import com.dao.StudentDao;
import com.entity.Student;

public class StudentService {
	private StudentDao dao = new StudentDao();
	public boolean validName(String name) {
	
		return dao.validName(name);
	}
	public boolean add(Student student) {
		
		return dao.add(student);
	}
	public List<Student> list() {
		
		return dao.list();
	}
	public Student getById(int id) {
		
		return dao.getById(id);
	}
	public boolean update(Student student) {
		
		return dao.update(student);
	}

}
