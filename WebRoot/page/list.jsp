<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'list.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
<link rel="stylesheet" href="css/index.css" type="text/css"></link>
  <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript">
  
  	$(function(){
  		$.post(
  			"${pageContext.request.contextPath}/student?method=list",
  			function(d){
  				var row="";
  				for(var i in d){
  					var s = d[i];
  					row+="<tr><td>"+s.id+"</td><td>"+s.name+"</td><td>"+s.age+"</td><td>"+s.birthday+"</td><td>"+s.hobbies+"</td><td>"+s.sex+"</td><td><img src='${pageContext.request.contextPath}/"+s.photoPath+"'/><td><button onclick='update("+s.id+")'>修改</button></td><td><button onclick='del("+s.id+")'>删除</button></td>"
  				}
  				$("#tb").append(row);
  			},
  			"json"
  		);
  	});
  	function update(id){
  	location.href="${pageContext.request.contextPath}/page/update.jsp?id="+id;
  	}
  </script>
   </head>
 <body>
   	<table id="tb">
   		<tr>
   			<th>序号</th>
   			<th>姓名</th>
   			<th>年龄</th>
   			<th>生日</th>
   			<th>爱好</th>
   			<th>性别</th>
   			<th>艳照</th>
   			<th>操作</th>
   		</tr>
   	</table>
  </body>
</html>