<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'add.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
  <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="js/jquery.metadata.js"></script>
  <script type="text/javascript" src="js/jquery.validate.js"></script>
  <script type="text/javascript">
  $(function(){
  		var id="${param.id}";
  		$.post(
  			"${pageContext.request.contextPath}/student?method=getById",
  			{id:id},
  			function(s){
  			
  				$("#id").val(s.id);
  				$("#photoPath").val(s.photo);
  				$("#name").val(s.name);
  				$("#birthday").val(s.birthday);
  				$("#age").val(s.age);
  				$("input[type='checkbox'][name='hobbies']").val(s.hobbies.split(","));
  				$("input[type='radio'][name='sex']").val([s.sex]);
  				$("#img").attr("src","${pageContext.request.contextPath}/"+s.photoPath);
  			},
  			"json"
  		);
   		$("#f1").validate({
  			errorPlacement:function(error,element){
  				if(element.is(":radio") || element.is(":checkbox")){
  					error.appendTo(element.parent());
  				}else{
  					error.insertAfter(element);
  				}
  			},
  			rules:{
  				age:{required:true,digits:true,range:[1,130]},
  				birthday:{required:true,dateISO:true},
  				hobbies:{required:true,minlength:2},
  				sex:{required:true},
  				photo:{accept:['.jpg','.png','.bmp','.gif']}
  			},
  			messages:{
  				age:{range:"年龄格式不正确"},
  				hobbies:{minlength:"至少选两项爱好"},
  				photo:{accept:"照片格式不正确"},
  			},
  			submitHandler:function(form){
  				var data=new FormData(form);
  				$.ajax({
  					url:"${pageContext.request.contextPath}/student?method=update",
  					type:"post",
  					data:data,
  					async:false,
  					cache:false,
  					contentType:false,
  					processData:false,
  					success:function(d){
  						if (d) {
							alert("修改成功");
							location.href="${pageContext.request.contextPath}/page/list.jsp";
							
						}else{
							alert("修改失败");
						}
  					},
  					dateType:"json"
  				});
  			}
  		}); 
  	});
  </script>
  </head>
  
  <body>
    <form id="f1">
   		<input type="hidden" name="id" id="id"/>
   		<input type="hidden" name="photoPath" id="photoPath"/>
    	<p>姓名:
    	<input type="text" name="name" id="name" readonly/>	
    	</p>
    	<p>年龄:
    	<input type="text" name="age" id="age"/>	
    	</p>
    	<p>生日:
    	<input type="text" name="birthday" id="birthday"/>	
    	</p>
    	<p>爱好:
    	<input type="checkbox" name="hobbies" value="吃"/>吃
    	<input type="checkbox" name="hobbies" value="喝"/>喝
    	<input type="checkbox" name="hobbies" value="嫖"/>嫖
    	<input type="checkbox" name="hobbies" value="赌"/>赌
    	<input type="checkbox" name="hobbies" value="抽"/>抽
		</p>
    	<p>爱好:
    	<input type="radio" name="sex" value="男"/>男
    	<input type="radio" name="sex" value="女"/>女
		</p>
    	<p>艳照:
    	<input type="file" name="photo" />
    	<img id="img" >
		</p>
    	<p>
    	<input type="submit" value="提交" />
		</p>
   </form>
  </body>
</html>