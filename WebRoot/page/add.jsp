<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'add.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
  <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="js/jquery.metadata.js"></script>
  <script type="text/javascript" src="js/jquery.validate.js"></script>
  <script type="text/javascript">
  	$(function(){
  		$("#f1").validate({
  			errorPlacement:function(error,element){
  				if(element.is(":radio") || element.is(":checkbox")){
  					error.appendTo(element.parent());
  				}else{
  					error.insertAfter(element);
  				}
  			},
  		rules:{
  				name:{required:true,rangelength:[2,8],remote:"${pageContext.request.contextPath}/student?method=validName"},
  				age:{required:true,digits:true,range:[1,130]},
  				birthday:{required:true,dateISO:true},
  				hobbies:{required:true,minlength:2},
  				sex:{required:true},
  				photo:{required:true,accept:['.jpg','.png','.bmp','.gif']}
  			},
  			messages:{
  				name:{rangelength:"名字至少是两个字"},
  				age:{range:"年龄格式不正确"},
  				hobbies:{minlength:"至少选两项爱好"},
  				photo:{accept:"照片格式不正确"},
  				name:{remote:"该用户已存在"}
  			},
  			submitHandler:function(form){
  				var data=new FormData(form);
  				$.ajax({
  					url:"${pageContext.request.contextPath}/student?method=add",
  					type:"post",
  					data:data,
  					async:false,
  					cache:false,
  					contentType:false,
  					processData:false,
  					success:function(d){
  						if (d) {
							alert("添加成功");
							location.href="${pageContext.request.contextPath}/page/list.jsp";
							
						}else{
							alert("添加失败");
						}
  					},
  					dateType:"json"
  				});
  			}
  		}); 
  	});
  </script>
  </head>
  
  <body>
    <form id="f1">
    	<p>姓名:
    	<input type="text" name="name"/>	
    	</p>
    	<p>年龄:
    	<input type="text" name="age"/>	
    	</p>
    	<p>生日:
    	<input type="text" name="birthday"/>	
    	</p>
    	<p>爱好:
    	<input type="checkbox" name="hobbies" value="吃"/>吃
    	<input type="checkbox" name="hobbies" value="喝"/>喝
    	<input type="checkbox" name="hobbies" value="嫖"/>嫖
    	<input type="checkbox" name="hobbies" value="赌"/>赌
    	<input type="checkbox" name="hobbies" value="抽"/>抽
		</p>
    	<p>爱好:
    	<input type="radio" name="sex" value="男"/>男
    	<input type="radio" name="sex" value="女"/>女
		</p>
    	<p>艳照:
    	<input type="file" name="photo" />
		</p>
    	<p>
    	<input type="submit" value="提交" />
		</p>
   </form>
  </body>
</html>